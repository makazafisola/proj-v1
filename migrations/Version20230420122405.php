<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230420122405 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO user (email, roles, password, firstname, lastname) VALUES ('admin@admin.com', '[\"ROLE_ADMIN\"]', '\$2y\$13\$geycxpkLdoVVMB4EuBNUXOe.l8F25Arn.xaGg9EXoXuIOn8ciiHTK', 'Admin', 'Admin')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
