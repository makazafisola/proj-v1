# All process to run the project

## Installation and configuration

- Create the new symfony project
```bash
symfony new proj-v1 --version=5.3.*
```

- Create the docker compose file and configure the database access
```bash
symfony console make:docker:database
```

- Run the server (using -d to run the command into the background)
```bash
docker-compose up -d
symfony serve -d
```

## Entity manager

- Install symfony flex to make installing dependencies easy
```bash
composer require symfony/flex 
```

- Install requirements via composer maker-bundle, orm-pack, security-bundle
```bash
composer require maker orm security 
``` 

- Create the user entity
```bash
symfony console make:user
```

- Create and fill the entities by running the following command for each entity
```bash
symfony console make:entity
```

- Using the following relation on the entity
> OneToOne : User - Patrimony
> ManyToMany : Patrimony - Service

## Make form and back office

- Installing EasyAdmin Bundle
```bash
composer require easycorp/easyadmin-bundle
```