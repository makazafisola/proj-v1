<?php

namespace App\Controller\Api;

use App\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


class ServiceController extends AbstractController
{
    /**
     * @Route("/api/services", name="api_services_list", methods={"GET"})
     */
    public function listServices(): JsonResponse
    {
        $services = $this->getDoctrine()->getRepository(Service::class)->findAll();

        $data = [];
        foreach ($services as $service) {
            $patrimonies = [];
            foreach ($service->getPatrimonies() as $patrimony) {
                $patrimonies[] = [
                    'id' => $patrimony->getId(),
                    'name' => $patrimony->getName(),
                    // Add any other patrimony attributes you want to expose in the API response
                ];
            }

            $data[] = [
                'id' => $service->getId(),
                'label' => $service->getLabel(),
                'sub_label' => $service->getSubLabel(),
                'description' => $service->getDescription(),
                'date_debut' => $service->getDateDebutPublication()->format('Y-m-d'),
                'date_fin' => $service->getDateFinPublication()->format('Y-m-d'),
                'patrimonies' => $patrimonies,
            ];
        }

        return new JsonResponse($data);
    }
}
